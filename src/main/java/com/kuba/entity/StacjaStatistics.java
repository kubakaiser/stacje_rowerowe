package com.kuba.entity;

import java.io.Serializable;

public class StacjaStatistics implements Serializable {

    private String nazwa;
    private Long liczbaStanowiskWolnych;
    private Long licztaStanowiskZajetych;
    private Long liczbaDostepnychRowerow;

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setLiczbaStanowiskWolnych(Long liczbaStanowiskWolnych) {
        this.liczbaStanowiskWolnych = liczbaStanowiskWolnych;
    }

    public void setLiczbaStanowiskZajetych(Long licztaStanowiskZajetych) {
        this.licztaStanowiskZajetych = licztaStanowiskZajetych;
    }

    public void setLiczbaDostepnychRowerow(Long liczbaDostepnychRowerow) {
        this.liczbaDostepnychRowerow = liczbaDostepnychRowerow;
    }

    public String getNazwa() {

        return nazwa;
    }

    public Long getLiczbaStanowiskWolnych() {
        return liczbaStanowiskWolnych;
    }

    public Long getLicztaStanowiskZajetych() {
        return licztaStanowiskZajetych;
    }

    public Long getLiczbaDostepnychRowerow() {
        return liczbaDostepnychRowerow;
    }
}
