package com.kuba.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
public class Stanowisko implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @OneToMany(mappedBy = "stanowisko")
    @JsonIgnore
    private Set<Rower> rowery;

    @ManyToOne
    private Stacja stacja;

    public Long getId() {
        return id;
    }

    public Set<Rower> getRowery() {
        return rowery;
    }

    public Stacja getStacja() {
        return stacja;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setRowery(Set<Rower> rowery) {
        this.rowery = rowery;
    }

    public void setStacja(Stacja stacja) {
        this.stacja = stacja;
    }
}
