package com.kuba.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Set;

@Entity
//@Data
//@NoArgsConstructor
//@AllArgsConstructor
public class Stacja implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String nazwa;

    @OneToMany (mappedBy = "stacja")
    @JsonIgnore
    private Set<Stanowisko> stanowiska;

    public Stacja() {
    }

    public Stacja(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setNazwa(String nazwa) {
        this.nazwa = nazwa;
    }

    public void setStanowiska(Set<Stanowisko> stanowiska) {
        this.stanowiska = stanowiska;
    }

    public Long getId() {

        return id;
    }

    public String getNazwa() {
        return nazwa;
    }

    public Set<Stanowisko> getStanowiska() {
        return stanowiska;
    }

}
