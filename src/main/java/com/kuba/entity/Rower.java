package com.kuba.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;

@Entity
public class Rower implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    Stanowisko stanowisko;

    public Long getId() {
        return id;
    }

    public Stanowisko getStanowisko() {
        return stanowisko;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setStanowisko(Stanowisko stanowisko) {
        this.stanowisko = stanowisko;
    }
}
