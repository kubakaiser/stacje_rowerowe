package com.kuba.Exception;

public class StacjaNotFoundException extends RuntimeException {
    public StacjaNotFoundException(String s) {
        super(s);
    }
}
