package com.kuba.controller;

import com.kuba.entity.StacjaStatistics;
import com.kuba.entity.Stanowisko;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import com.kuba.Exception.StacjaNotFoundException;
import com.kuba.dao.StacjaRepository;
import com.kuba.entity.Stacja;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RestController
public class StacjaController {

    @Autowired
    private StacjaRepository stacjaRepository;

    @GetMapping("/stacje")
    public List<Stacja> getAllStations() {
        return stacjaRepository.findAll();
    }

    @GetMapping("/stacje/{id}")
    public Stacja getStationByID(@PathVariable Long id) {
        Optional<Stacja> stacja = stacjaRepository.findById(id);
        if (!stacja.isPresent()) {
            throw new StacjaNotFoundException("id: " + id);
        }
        return stacja.get();
    }

    @DeleteMapping("stacje/{id}")
    public void deleteStation(@PathVariable Long id) {
        stacjaRepository.deleteById(id);
    }

    @PostMapping("/stacje")
    public ResponseEntity<Object> createStation(@RequestBody Stacja stacja) {
        Stacja newStacja = stacjaRepository.save(stacja);
        URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(newStacja.getId()).toUri();
        return ResponseEntity.created(location).build();
    }

    @PutMapping("/stacje/{id}")
    public ResponseEntity<Object> updateStation(@RequestBody Stacja stacja, @PathVariable Long id) {
        Optional<Stacja> updateStacja = stacjaRepository.findById(id);
        if (!updateStacja.isPresent())
            return ResponseEntity.notFound().build();
        stacja.setId(id);
        stacjaRepository.save(stacja);
        return ResponseEntity.noContent().build();
    }

    @GetMapping("/statistics")
    public List<StacjaStatistics> getStatistics(){
        List<StacjaStatistics> result = new ArrayList<>();
        List<Stacja> stacje = stacjaRepository.findAll();

        for (Stacja stacja : stacje) {
            StacjaStatistics stacjaStatistics = new StacjaStatistics();

            Long liczbaStanowiskWolnych = 0L;
            Long liczbaStanowiskZajetych = 0L;
            Long liczbaDostepnychRowerow = 0L;

            Set<Stanowisko> stanowiska = stacja.getStanowiska();

            for (Stanowisko stanowisko : stanowiska) {
                int liczbaRowerowNaStanowisku = stanowisko.getRowery().size();
                if (liczbaRowerowNaStanowisku == 0) {
                    liczbaStanowiskWolnych++;
                } else {
                    liczbaStanowiskZajetych++;
                }
                liczbaDostepnychRowerow += liczbaRowerowNaStanowisku;
            }

            stacjaStatistics.setNazwa(stacja.getNazwa());
            stacjaStatistics.setLiczbaStanowiskWolnych(liczbaStanowiskWolnych);
            stacjaStatistics.setLiczbaStanowiskZajetych(liczbaStanowiskZajetych);
            stacjaStatistics.setLiczbaDostepnychRowerow(liczbaDostepnychRowerow);

            result.add(stacjaStatistics);
        }
        return result;
    }

}
