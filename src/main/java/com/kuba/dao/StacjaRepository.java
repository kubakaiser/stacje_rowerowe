package com.kuba.dao;

import com.kuba.entity.Stacja;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StacjaRepository extends JpaRepository<Stacja, Long> {
}
