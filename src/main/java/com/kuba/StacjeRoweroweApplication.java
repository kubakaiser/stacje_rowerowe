package com.kuba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class StacjeRoweroweApplication {

	public static void main(String[] args) {
		SpringApplication.run(StacjeRoweroweApplication.class, args);

	}
}
